package com.xxll.config.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;


public class RedisConfig {

    /**
     * 使用RedisTemplate<String, Object> 是需要注意其序列化的方式
     * @param redisConnectionFactory
     * @param redisSerializer
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory, RedisSerializer<Object> redisSerializer) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //redis Key 序列化
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //redis Value 序列化
        redisTemplate.setValueSerializer(redisSerializer);
        //redis HashKey 序列化
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        //redis HashValue 序列化
        redisTemplate.setHashValueSerializer(redisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    /**
     * 更换redis的序列化形式为Jackson
     * @param objectMapper
     * @return
     */
    @Bean
    public RedisSerializer<Object> redisSerializer(ObjectMapper objectMapper) {
        //创建JSON序列化器
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
        serializer.setObjectMapper(objectMapper);
        return serializer;
    }

}
