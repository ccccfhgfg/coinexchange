package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 根据用户id查询该用户所拥有的菜单
     * @param userId
     * @return
     */
    List<SysMenu> selectMenusByUserId(@Param("userId") Long userId);
}