package com.xxll.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxll.domain.AdminBank;
import com.baomidou.mybatisplus.extension.service.IService;
public interface AdminBankService extends IService<AdminBank>{


    /**
     * 条件查询公司银行卡
     * @param page 分页参数
     * @param bankCard  公司的银行卡
     **/
    Page<AdminBank> findByPage(Page<AdminBank> page, String bankCard);
}
