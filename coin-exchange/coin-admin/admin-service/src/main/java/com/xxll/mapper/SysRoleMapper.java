package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysRole;
import org.apache.ibatis.annotations.Param;

public interface SysRoleMapper extends BaseMapper<SysRole> {
    /**
     * 根据用户id查询角色
     * @param userId
     * @return
     */
    String getUserRoleCode(@Param("userId") Long userId);
}