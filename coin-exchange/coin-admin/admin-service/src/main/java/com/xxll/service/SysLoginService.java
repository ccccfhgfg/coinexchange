package com.xxll.service;

import com.xxll.model.LoginResult;
/**
 * <h2>登录接口</h2>
 * @author zeny
 **/
public interface SysLoginService {

    /**
     * 登录的实现
     * @param username
     * @param password
     **/
    LoginResult login(String username, String password);
}
