package com.xxll.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxll.domain.SysRole;
import com.xxll.mapper.SysRoleMapper;
import com.xxll.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * <h2>判断用户是否是超级管理员</h2>
     *
     * @param userId
     **/
    @Override
    public boolean isSuperAdmin(Long userId) {
        // 当用户的角色code 为：ROLE_ADMIN 时，该用户为超级的管理员
        // 用户的id->用户的角色->该角色的Code是否为ROLE_ADMIN
        String roleCode = sysRoleMapper.getUserRoleCode(userId);
        return !StringUtils.isEmpty(roleCode) && "ROLE_ADMIN".equals(roleCode);
    }

    /**
     * <h2>使用角色的名称模糊分页角色查询</h2>
     *
     * @param page 分页数据
     * @param name 条件
     **/
    @Override
    public Page<SysRole> findByPage(Page<SysRole> page, String name) {
        return page(page, new LambdaQueryWrapper<SysRole>()
                .like(!StringUtils.isEmpty(name), SysRole::getName, name)
        );
    }
}
